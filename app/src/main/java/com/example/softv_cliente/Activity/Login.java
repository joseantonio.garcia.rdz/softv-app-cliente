package com.example.softv_cliente.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.softv_cliente.Models.LoginModel;
import com.example.softv_cliente.R;
import com.example.softv_cliente.Request.Request;
import com.example.softv_cliente.Util.BarraCarga;
import com.example.softv_cliente.Util.Util;

import org.json.JSONObject;

public class Login extends AppCompatActivity {

    EditText usuarioEdt, contrasenaEdt;
    Button entrar;
    ImageButton viewPassword;
    public String usuario,contrasena;
    private ProgressDialog dialogLogin;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        final Request Rqs = new Request();
        usuarioEdt=(EditText)findViewById(R.id.usuario);
        contrasenaEdt = (EditText)findViewById(R.id.contrasenia);
        viewPassword = (ImageButton)findViewById(R.id.viewPassword);
        entrar = (Button)findViewById(R.id.btnLogin);
        dialogLogin = new BarraCarga().showDialog(this);



        viewPassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        contrasenaEdt.setInputType(InputType.TYPE_CLASS_TEXT);
                        contrasenaEdt.setSelection(contrasenaEdt.getText().length());

                        //et.setSelection(et.getText().length());
                        break;
                    case MotionEvent.ACTION_UP:
                        contrasenaEdt.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        contrasenaEdt.setSelection(contrasenaEdt.getText().length());
                }
                return false;
            }
        });

        entrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogLogin.show();
                String user, enco;
                usuario = usuarioEdt.getText().toString();
                contrasena = contrasenaEdt.getText().toString();
                if (usuarioEdt.getText().toString().length() == 0) {
                    Toast.makeText(getApplicationContext(), "Introduzca usuario", Toast.LENGTH_LONG).show();
                    dialogLogin.dismiss();
                } else {
                    //varifica que el campo contraseña este lleno
                    if (contrasenaEdt.getText().toString().length() == 0) {
                        Toast.makeText(getApplicationContext(), "Introduzca contraseña", Toast.LENGTH_LONG).show();
                        dialogLogin.dismiss();
                    } else {
                        //verifica que el usuario tenga internet
                        if (!isOnline()) {
                            Toast.makeText(getApplicationContext(), "No cuenta con conexión a Internet", Toast.LENGTH_LONG).show();
                            dialogLogin.dismiss();
                        } else {
                            //se encadena el usuario y contraseña
                            user = usuario + ":" + contrasena;
                            //se codifica la cadena de texto en base64
                            enco = (android.util.Base64.encodeToString(user.getBytes(), android.util.Base64.NO_WRAP));
                            //se manda el campo usuario y enco para guardarlos en el SharedPreferences
                            guardarPre(usuario, enco);
                            //manda request del login
                            Rqs.getLogin(Login.this,dialogLogin);
                            //inicia el dialog de carga

                            /////////////
                        }

                    }
                }



            }
        });


    }
    public void guardarPre(String usario, String encode) {
        //se inicia el preferences
        Util.preferences = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        Util.editor = Util.preferences.edit();
        //se guarda el usario
        Util.editor.putString("usuario", usario);
        //se guarda la cadena en base64
        Util.editor.putString("enco", "Basic: " + encode);
        Util.editor.commit();
    }
    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();
    }
}
