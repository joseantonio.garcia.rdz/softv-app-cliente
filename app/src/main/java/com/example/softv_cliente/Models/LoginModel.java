package com.example.softv_cliente.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginModel {
   public Integer Contrato;
   public String Usuario;
   public String token;

    public LoginModel(Integer contrato, String usuario, String token) {
        this.Contrato = contrato;
        this.Usuario = usuario;
        this.token = token;
    }
}
