package com.example.softv_cliente.Request;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.util.JsonToken;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.softv_cliente.Activity.Inicio;
import com.example.softv_cliente.Activity.Login;
import com.example.softv_cliente.Models.LoginModel;
import com.example.softv_cliente.Service.Services;
import com.example.softv_cliente.Util.Service;
import com.example.softv_cliente.Util.Util;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static java.util.Arrays.asList;

public class Request {
    Services services = new Services();


    public void ErrorMensaje(final Context context,final String error){
        Toast.makeText(context, error, Toast.LENGTH_LONG).show();
    }

    public void getLogin(final Context context, final ProgressDialog dialogLogin) {
        Services restApiAdapter = new Services();
        Service service = restApiAdapter.getClientService(context);
        Call<JsonObject> call = service.getLogin();
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == 200) {
                    Util.preferences = context.getSharedPreferences("credenciales", Context.MODE_PRIVATE);
                    Util.editor = Util.preferences.edit();
                    JsonObject userJson = response.body().getAsJsonObject("LogOnResult");
                    LoginModel loginModel = new LoginModel(
                            userJson.get("Contrato").getAsInt(),
                            userJson.get("Usuario").getAsString(),
                            userJson.get("token").getAsString()

                    );
                    Util.editor.putString("token", loginModel.token);
                    Util.editor.putInt("contrato", loginModel.Contrato);
                    Util.editor.commit();
                    try{
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("Contrato",Util.getContrato(Util.preferences));
                        jsonObject.put("Token", FirebaseInstanceId.getInstance().getToken());
                        addToken(context,jsonObject,dialogLogin);
                    }catch (Exception e){}

                } else {
                    dialogLogin.dismiss();
                    ErrorMensaje(context,"Error: "+response.message());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                dialogLogin.dismiss();
                ErrorMensaje(context,"Error "+t.getMessage());
            }
        });
    }
    public void addToken(final Context context, final JSONObject jsonObject, final ProgressDialog dialogLogin) {
        Call<JsonObject> call = services.RequestPost(jsonObject).addToken();
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == 200) {
                    String userJson = response.body().getAsJsonObject("AddClienteTokenResult").get("Añadido").toString();
                    if(userJson.equals("1")){
                        Intent intento = new Intent(context, Inicio.class);
                        intento.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        context.startActivity(intento);
                        dialogLogin.dismiss();
                    }

                } else {
                    ErrorMensaje(context,"Error "+response.message());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                ErrorMensaje(context,"Error "+t.getMessage());

            }
        });
    }

}
