package com.example.softv_cliente.Util;

import com.example.softv_cliente.Constants.Constants;
import com.example.softv_cliente.Models.LoginModel;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface Service {
    @GET(Constants.URL_LOGIN)
    Call<JsonObject> getLogin();

    @POST(Constants.URL_ADD_TOKEN)
    Call<JsonObject> addToken();
}
