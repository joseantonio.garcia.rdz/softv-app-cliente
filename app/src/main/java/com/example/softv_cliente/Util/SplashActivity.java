package com.example.softv_cliente.Util;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;

import com.example.softv_cliente.Activity.Inicio;
import com.example.softv_cliente.Activity.Login;

public class SplashActivity  extends AppCompatActivity {

    private SharedPreferences preferences;
    public static boolean LoginShare=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        preferences = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        Intent intentLogin = new Intent(this, Login.class);
        Intent intentinicio = new Intent(this, Inicio.class);
        if (!TextUtils.isEmpty(Util.getTokenPreference(preferences))) {
            LoginShare=true;
            intentinicio.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intentinicio);

        } else {
            intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intentLogin);
            LoginShare=false;
        }
        finish();
    }
}