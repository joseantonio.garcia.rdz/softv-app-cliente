package com.example.softv_cliente.Util;

import android.content.SharedPreferences;

public class Util {
    public static SharedPreferences preferences;
    public static SharedPreferences.Editor editor;

    public static String getTokenPreference(SharedPreferences preferences){
        return preferences.getString("token", "");
    }
    public static String getEncoPreference(SharedPreferences preferences){
        return preferences.getString("enco", "");
    }

    public static String getUsuarioPreference(SharedPreferences preferences){
        return preferences.getString("usuario", "");
    }
    public static int getContrato(SharedPreferences preferences){
        return preferences.getInt("contrato", 0);

    }
}
